package web

import (
	"bytes"
	"context"
	"errors"
	"gitee.com/zhangyu520cw/webook/internal/domain"
	"gitee.com/zhangyu520cw/webook/internal/service"
	svcmocks "gitee.com/zhangyu520cw/webook/internal/service/mocks"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/mock/gomock"
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	// DebugMode indicates gin mode is debug.
	DebugMode = "debug"
	// ReleaseMode indicates gin mode is release.
	ReleaseMode = "release"
	// TestMode indicates gin mode is test.
	TestMode = "test"
)

func TestUserHandler_SignUp(t *testing.T) {
	gin.SetMode(TestMode)
	testCases := []struct {
		name string

		mock func(ctrl *gomock.Controller) (service.UserService, service.CodeService)
		// 构造请求，预期中的输入
		reqBuilder func(t *testing.T) *http.Request

		//预期响应
		wantCode int
		wantBody string
	}{
		{
			name: "注册成功",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userSvc := svcmocks.NewMockUserService(ctrl)
				userSvc.EXPECT().Signup(gomock.Any(), domain.User{
					Email:    "123@qq.com",
					Password: "Anmail@123Password",
				}).Return(nil)
				codeSvc := svcmocks.NewMockCodeService(ctrl)
				return userSvc, codeSvc
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost, "/users/signup", bytes.NewReader([]byte(`{
"email":"123@qq.com",
"password":"Anmail@123Password",
"confirmPassword":"Anmail@123Password"
}`)))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "注册成功",
		},
		{
			name: "Bind绑定失败",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				//userSvc := svcmocks.NewMockUserService(ctrl)
				//codeSvc := svcmocks.NewMockCodeService(ctrl)
				return nil, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost, "/users/signup", bytes.NewReader([]byte("lals")))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusBadRequest,
		},
		{
			name: "邮箱格式不对",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				//userSvc := svcmocks.NewMockUserService(ctrl)
				//codeSvc := svcmocks.NewMockCodeService(ctrl)
				return nil, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost, "/users/signup", bytes.NewReader([]byte(`{
"email":"123.com",
"password":"Anmail@123Password",
"confirmPassword":"Anmail@123Password"
}`)))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "非法邮箱格式",
		},
		{
			name: "两次密码输入不相同",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				//userSvc := svcmocks.NewMockUserService(ctrl)
				//codeSvc := svcmocks.NewMockCodeService(ctrl)
				return nil, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost, "/users/signup", bytes.NewReader([]byte(`{
"email":"123@qq.com",
"password":"Anmail@123Password",
"confirmPassword":"Anmail@123Password11"
}`)))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "两次输入密码不对",
		},
		{
			name: "密码格式不对",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				//userSvc := svcmocks.NewMockUserService(ctrl)
				//codeSvc := svcmocks.NewMockCodeService(ctrl)
				return nil, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost, "/users/signup", bytes.NewReader([]byte(`{
"email":"123@qq.com",
"password":"123456",
"confirmPassword":"123456"
}`)))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "密码必须包含字母、数字、特殊字符，并且不少于八位",
		},
		{
			name: "邮箱冲突",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userSvc := svcmocks.NewMockUserService(ctrl)
				userSvc.EXPECT().Signup(gomock.Any(), domain.User{
					Email:    "123@qq.com",
					Password: "Anmail@123Password",
				}).Return(service.ErrDuplicateEmail)
				codeSvc := svcmocks.NewMockCodeService(ctrl)
				return userSvc, codeSvc
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost, "/users/signup", bytes.NewReader([]byte(`{
"email":"123@qq.com",
"password":"Anmail@123Password",
"confirmPassword":"Anmail@123Password"
}`)))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "邮箱冲突，请换一个",
		},
		{
			name: "系统错误",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userSvc := svcmocks.NewMockUserService(ctrl)
				userSvc.EXPECT().Signup(gomock.Any(), domain.User{
					Email:    "123@qq.com",
					Password: "Anmail@123Password",
				}).Return(errors.New("模拟系统异常"))
				codeSvc := svcmocks.NewMockCodeService(ctrl)
				return userSvc, codeSvc
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost, "/users/signup", bytes.NewReader([]byte(`{
"email":"123@qq.com",
"password":"Anmail@123Password",
"confirmPassword":"Anmail@123Password"
}`)))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "系统错误",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			userSvc, CodeSvc := tc.mock(ctrl)
			hdl := NewUserHandler(userSvc, CodeSvc)

			//注册路由
			server := gin.Default()
			hdl.RegisterRoutes(server)

			//准备请求
			req := tc.reqBuilder(t)

			//准备记录响应
			recorder := httptest.NewRecorder()

			//执行
			server.ServeHTTP(recorder, req)

			//断言
			assert.Equal(t, tc.wantCode, recorder.Code)
			assert.Equal(t, tc.wantBody, recorder.Body.String())
		})
	}
}

func TestUserEmailPattern(t *testing.T) {
	testCases := []struct {
		name  string
		email string
		match bool
	}{
		{
			name:  "正确邮箱",
			email: "123456@163.com",
			match: true,
		},
	}
	h := NewUserHandler(nil, nil)

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			match, err := h.emailRexExp.MatchString(tc.email)
			require.NoError(t, err)
			assert.Equal(t, tc.match, match)
		})
	}
}

//func TestHTTP(t *testing.T) {
//	req, err := http.NewRequest(http.MethodPost, "users/signup", bytes.NewReader([]byte("我的请求体")))
//	assert.NoError(t, err)
//	fmt.Println(req)
//	recoreder := httptest.NewRecorder()
//	assert.Equal(t, http.StatusOK, recoreder.Code)
//
//	svc := service.NewUserService()
//	h := NewUserHandler(nil, nil)
//}

func TestMock(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// mock实现，模拟实现
	userSvc := svcmocks.NewMockUserService(ctrl)
	//设置了模拟场景
	userSvc.EXPECT().Signup(gomock.Any(), domain.User{
		Id:    1,
		Email: "123@qq.com",
	}).Return(errors.New("DB出错"))
	err := userSvc.Signup(context.Background(), domain.User{
		Id:    1,
		Email: "123@qq.com",
	})
	t.Log(err)
}
