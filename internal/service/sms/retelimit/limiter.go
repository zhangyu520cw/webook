package retelimit

import (
	"context"
	"errors"
	"gitee.com/zhangyu520cw/webook/internal/service/sms"
	"gitee.com/zhangyu520cw/webook/pkg/limiter"
)

var errLimited = errors.New("触发了限流")

type RateLimitSMSService struct {
	//被装饰的
	svc     sms.Service
	limiter limiter.Limiter
	key     string
}

func (r RateLimitSMSService) Send(ctx context.Context, tplId string, args []string, numbers ...string) error {
	limiterd, err := r.limiter.Limit(ctx, r.key)
	if err != nil {
		return err
	}
	if limiterd {
		return errLimited
	}
	return r.svc.Send(ctx, tplId, args, numbers...)
}

func NewRateLimitSMSService(svc sms.Service, limiter limiter.Limiter, key string) *RateLimitSMSService {
	return &RateLimitSMSService{
		svc:     svc,
		limiter: limiter,
		key:     "sms-limiter",
	}
}
