package service

import (
	"context"
	"errors"
	"gitee.com/zhangyu520cw/webook/internal/domain"
	"gitee.com/zhangyu520cw/webook/internal/repository"
	repomocks "gitee.com/zhangyu520cw/webook/internal/repository/mocks"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"golang.org/x/crypto/bcrypt"
	"testing"
)

func TestPasswordEncrypt(t *testing.T) {
	password := []byte("123456#hello")
	encrypted, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	assert.NoError(t, err)
	println(string(encrypted))
	err = bcrypt.CompareHashAndPassword(encrypted, []byte("123456#hello"))
	assert.NoError(t, err)
}

func Test_userService_Login(t *testing.T) {
	testCases := []struct {
		name     string
		mock     func(ctrl *gomock.Controller) repository.UserRepository
		ctx      context.Context
		email    string
		password string

		wantUser domain.User
		wantErr  error
	}{
		{
			name: "登录成功",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				repo := repomocks.NewMockUserRepository(ctrl)
				repo.EXPECT().FindByEmail(gomock.Any(), "123456@qq.com").Return(domain.User{
					Email:    "123456@qq.com",
					Password: "$2a$10$OsJLnvq1wL7x5ihlDpkhLePaAxmyK.6A3QRsZFom2Rckj1UhNWYEm",
				}, nil)
				return repo
			},
			email:    "123456@qq.com",
			password: "123456#hello",
			wantUser: domain.User{
				Email:    "123456@qq.com",
				Password: "$2a$10$OsJLnvq1wL7x5ihlDpkhLePaAxmyK.6A3QRsZFom2Rckj1UhNWYEm",
			},
			wantErr: nil,
		},
		{
			name: "用户不存在",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				repo := repomocks.NewMockUserRepository(ctrl)
				repo.EXPECT().FindByEmail(gomock.Any(), "123456@qq.com").Return(
					domain.User{}, repository.ErrUserNotFound)
				return repo
			},
			email:    "123456@qq.com",
			password: "123456#hello",
			wantUser: domain.User{},
			wantErr:  ErrInvalidUserOrPassword,
		},
		{
			name: "系统错误",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				repo := repomocks.NewMockUserRepository(ctrl)
				repo.EXPECT().FindByEmail(gomock.Any(), "123456@qq.com").Return(
					domain.User{}, errors.New("db错误"))
				return repo
			},
			email:    "123456@qq.com",
			password: "123456#hello",
			wantUser: domain.User{},
			wantErr:  errors.New("db错误"),
		},

		{
			name: "密码错误",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				repo := repomocks.NewMockUserRepository(ctrl)
				repo.EXPECT().FindByEmail(gomock.Any(), "123456@qq.com").Return(domain.User{
					Email:    "123456@qq.com",
					Password: "$2a$10$OsJLnvq1wL7x5ihlDpkhLePaAxmyK.6A3QRsZFom2Rckj1UhNWYEm",
				}, nil)
				return repo
			},
			email:    "123456@qq.com",
			password: "want@123456",
			wantUser: domain.User{},
			wantErr:  ErrInvalidUserOrPassword,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			repo := tc.mock(ctrl)
			svc := NewUserService(repo)
			user, err := svc.Login(tc.ctx, tc.email, tc.password)
			if err != nil {
				assert.Equal(t, tc.wantErr, err)
			}
			assert.Equal(t, tc.wantUser, user)
		})
	}
}

func TestUserService_FindById(t *testing.T) {
	testCases := []struct {
		name     string
		mock     func(ctrl *gomock.Controller) repository.UserRepository
		ctx      context.Context
		uid      int64
		wantUser domain.User
		wantErr  error
	}{
		{
			name: "用户存在",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				repo := repomocks.NewMockUserRepository(ctrl)
				repo.EXPECT().FindById(gomock.Any(), int64(11)).Return(domain.User{Id: 11}, nil)
				return repo
			},
			uid:      11,
			wantUser: domain.User{Id: 11},
			wantErr:  nil,
		},
		{
			name: "用户不存在",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				repo := repomocks.NewMockUserRepository(ctrl)
				repo.EXPECT().FindById(gomock.Any(), int64(11)).Return(domain.User{}, errors.New("用户不存在"))
				return repo
			},
			uid:      11,
			wantUser: domain.User{},
			wantErr:  errors.New("用户不存在"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			repo := tc.mock(ctrl)
			svc := NewUserService(repo)
			user, err := svc.FindById(tc.ctx, tc.uid)
			assert.Equal(t, tc.wantErr, err)
			assert.Equal(t, tc.wantUser, user)
		})
	}
}
