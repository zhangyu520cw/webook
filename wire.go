//go:build wireinject

package main

import (
	"gitee.com/zhangyu520cw/webook/internal/repository"
	"gitee.com/zhangyu520cw/webook/internal/repository/cache"
	"gitee.com/zhangyu520cw/webook/internal/repository/dao"
	"gitee.com/zhangyu520cw/webook/internal/service"
	"gitee.com/zhangyu520cw/webook/internal/web"
	"gitee.com/zhangyu520cw/webook/ioc"
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
	"webook/ioc"
)

func InitWebServer() *gin.Engine {
	wire.Build(
		// 第三方依赖
		ioc.InitRedis, ioc.InitDB,
		// DAO 部分
		dao.NewUserDAO,

		// cache 部分
		cache.NewCodeCache, cache.NewUserCache,

		// repository 部分
		repository.NewCachedUserRepository,
		repository.NewCodeRepository,

		// Service 部分
		ioc.InitSMSService,
		service.NewUserService,
		service.NewCodeService,

		// handler 部分
		web.NewUserHandler,

		ioc.InitGinMiddlewares,
		ioc.InitWebServer,
	)
	return gin.Default()
}
